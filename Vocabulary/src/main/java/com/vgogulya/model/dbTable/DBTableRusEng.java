package com.vgogulya.model.dbTable;

public class DBTableRusEng implements IDBTable {

	@Override
	public String getQuerySelect(){
		return "SELECT word, translation from RusEngWords WHERE word = ?";
	}
	
	@Override 
	public String getQueryInsert(){
		return "INSERT INTO RusEngWords(word, translation) VALUES(?, ?)";
	}
	
	@Override
	public String getQueryDelete(){
		return "DELETE from RusEngWords WHERE word = ? AND translation = ?";
	}
	
	@Override
	public String getQueryDeleteAll(){
		return "DELETE from RusEngWords WHERE word = ?";
	}
	
	@Override
	public eDBTableType getType(){
		return eDBTableType.RUS_ENG;
	}
}
