package com.vgogulya.model.dbTable;

public class DBTableEngRus implements IDBTable {

	@Override
	public String getQuerySelect(){
		return "SELECT word, translation from EngRusWords WHERE word = ?";
	}
	
	@Override 
	public String getQueryInsert(){
		return "INSERT INTO EngRusWords(word, translation) VALUES(?, ?)";
	}
	
	@Override
	public String getQueryDelete(){
		return "DELETE from EngRusWords WHERE word = ? AND translation = ?";
	}
	
	@Override
	public String getQueryDeleteAll(){
		return "DELETE from EngRusWords WHERE word = ?";
	}
	
	@Override
	public eDBTableType getType(){
		return eDBTableType.ENG_RUS;
	}
}
