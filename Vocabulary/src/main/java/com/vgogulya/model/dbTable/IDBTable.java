package com.vgogulya.model.dbTable;

public interface IDBTable {
	
	String getQuerySelect();
	String getQueryInsert();
	String getQueryDelete();
	String getQueryDeleteAll();
	
	eDBTableType getType();
}