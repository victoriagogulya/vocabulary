package com.vgogulya.model;

public final class Constants {
	
	//SQLite DB
	public static final String JDBC_SQLITE_PATH = "jdbc:sqlite:";
	public static final String DB_SQLITE_DRIVER = "org.sqlite.JDBC";
	public static final String DB_SQLITE_NAME = "VocabularySQLiteDB.s3db";
	
	//Code point ENG
	public static final int CODE_POINT_MIN_UPPER_CASE_ENG = 65;
	public static final int CODE_POINT_MAX_UPPER_CASE_ENG = 90;
	
	public static final int CODE_POINT_MIN_LOWER_CASE_ENG = 97;
	public static final int CODE_POINT_MAX_LOWER_CASE_ENG = 122;
	
	public static final String CHARSET_CP1251 = "Cp1251";
	public static final String CHARSET_UTF8 = "UTF-8";
	
	public static final String SPLITTER_N = "\n";
}
