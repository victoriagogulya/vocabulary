package com.vgogulya.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vgogulya.model.dbTable.DBTableEngRus;
import com.vgogulya.model.dbTable.DBTableRusEng;
import com.vgogulya.model.dbTable.IDBTable;
import com.vgogulya.model.dbTable.eDBTableType;

public class Database {
	
	private Connection conn = null;
	private IDBTable currentTable = new DBTableEngRus();
	
	
	public void setCurrentDBTable(eDBTableType tableType){
		if(null == currentTable || tableType != currentTable.getType()){
			switch(tableType){
			case ENG_RUS:
				currentTable = new DBTableEngRus();
				break;
			case RUS_ENG:
				currentTable = new DBTableRusEng();
				break;
			}
		}
	}
	
	public boolean connect()
	{
		boolean result = true;
		try {
			Class.forName(Constants.DB_SQLITE_DRIVER);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			result = false;
		}
		String dbUrl = Constants.JDBC_SQLITE_PATH + Constants.DB_SQLITE_NAME;
		try {
			conn = DriverManager.getConnection(dbUrl);
		} catch (SQLException e) {
			result = false;
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean closeConnection(){
		boolean result = false;
		
		try {
			if(!conn.isClosed()){
				conn.close();
				result = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public int insert(String word, List<String> translationList){
		int insertedRows = 0;
		
		try {
			if(!conn.isClosed()){
				filterAlreadyExistsEngRus(word, translationList);
				
				String query = currentTable.getQueryInsert();
				for(String str : translationList){
					try(PreparedStatement preparedStatement = conn.prepareStatement(query)){
						preparedStatement.setString(1, word);
						preparedStatement.setString(2, str);
						insertedRows += preparedStatement.executeUpdate();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return insertedRows;
	}
	
	public List<String> select(String word){
		List<String> translationList = new ArrayList<>();
		
		try {
			if(!conn.isClosed())
			{
				translationList = selectOpenedConnection(word);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return translationList;
	}
	
	public int delete(String word, String translation){
		int deletedRows = 0;
		List<String> translationList = selectOpenedConnection(word);
		
		if(null != translationList && 
		   !translationList.isEmpty() && 
		   translationList.contains(translation)){
			try {
				if(!conn.isClosed()){
					String query = currentTable.getQueryDelete();
					try(PreparedStatement preparedStatement = conn.prepareStatement(query)){
						preparedStatement.setString(1, word);
						preparedStatement.setString(2, translation);
				        
				        deletedRows = preparedStatement.executeUpdate();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
		return deletedRows;
	}
	
	public int deleteAll(String word){
		int deletedRows = 0;
		List<String> translationList = selectOpenedConnection(word);
		
		if(null != translationList && 
		   !translationList.isEmpty()){
			
			try {
				if(!conn.isClosed()){
					String query = currentTable.getQueryDeleteAll();
					try(PreparedStatement preparedStatement = conn.prepareStatement(query)){
						preparedStatement.setString(1, word);
				        
				        deletedRows = preparedStatement.executeUpdate();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	
		return deletedRows;
	}
	
	private  List<String> selectOpenedConnection(String word){
		List<String> translationList = new ArrayList<>();
		
		try {
			if(!conn.isClosed())
			{
				String query = currentTable.getQuerySelect();
				try(PreparedStatement preparedStatement = conn.prepareStatement(query)) {
					preparedStatement.setString(1, word);
			        ResultSet rs = null;
			        rs = preparedStatement.executeQuery();
			        while(rs.next()) {
			            translationList.add(rs.getString(2));
			        }
				}
				catch(SQLException e){
				   e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return translationList;
	}
	
	private void filterAlreadyExistsEngRus(String word, List<String> translationList){
		List<String> wordsInDB = selectOpenedConnection(word);
		for(String str : wordsInDB){
			if(translationList.contains(str)){
            	translationList.remove(str);
            }
		}
	}

}
