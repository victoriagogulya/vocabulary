package com.vgogulya.model;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Word {
	
	public static boolean isCorrect(String word){
		return !word.isEmpty() && convert(word) && areAllLetters(word);
	}
	
	public static boolean areCorrectAll(List<String> words){
		boolean result = true;
		
		if(words.isEmpty()){
			result = false;
		}
		
		for(int i = 0; i < words.size() && result; i++){
			result = isCorrect(words.get(i));
		}
		return result;
	}
	
	public static boolean isEnglish(String word){	
		boolean result = true;
		
		for(int i = 0; i < word.length() && result; ++i) {
			int codePoint = word.codePointAt(i);
			if(codePoint < Constants.CODE_POINT_MIN_UPPER_CASE_ENG || 
					(codePoint > Constants.CODE_POINT_MAX_UPPER_CASE_ENG && 
							codePoint < Constants.CODE_POINT_MIN_LOWER_CASE_ENG) || 
					codePoint > Constants.CODE_POINT_MAX_LOWER_CASE_ENG) {
				result = false;
			}
		}
		return result;
	}
	
	public static String toLowerCase(String word){
		return word.toLowerCase();
	}
	
	public static List<String> toLowerCaseAll(List<String> words){
		List<String> wordsLowerCase = new ArrayList<>();
		
		for(String str : words){
			wordsLowerCase.add(str.toLowerCase());
		}
		return wordsLowerCase;
	}
	
	public static List<String> splitToWords(String text, String splitter){
		String[] strArr = text.split(splitter);
		List<String> listOfWords = Arrays.asList(strArr);
		return listOfWords;
	}
	
	//private
	private static boolean convert(String word){
		boolean result = true;
		try {
			word = new String(word.getBytes(Constants.CHARSET_CP1251), Constants.CHARSET_UTF8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			System.out.println("model.Word.isCorrect() - Exception! Couldn't convert");
			result = false;
		}
		return result;
	}
	
	private static boolean areAllLetters(String word){
		boolean result = true;
		for(int i = 0; i < word.length() && result; ++i) {
			if(!Character.isLetter(word.charAt(i))){
				result = false;
			}
		}
		return result;
	}
}
