package com.vgogulya.view;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Utility {

	public static void showAlertInfo(String message) { 
	    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	    alert.setTitle("Message Here...");
	    alert.setHeaderText("Look, an Information Dialog");
	    alert.setContentText(message);
	    alert.showAndWait();
	}
	
	public static eAlertResult showAlertConfirmation(String message) { 
	    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    alert.setTitle("Attention..");
	    alert.setHeaderText("Press OK or Cancel");
	    alert.setContentText(message);
	    alert.showAndWait();
	    
	    return convertButtontTypeToAlertResult(alert.getResult());
	}
	
	private static eAlertResult convertButtontTypeToAlertResult(ButtonType buttonType){
		eAlertResult alertResult = eAlertResult.UNKNOWN;
		
		if(ButtonType.OK == buttonType){
			alertResult = eAlertResult.OK;
		}
		else if(ButtonType.CANCEL == buttonType){
			alertResult = eAlertResult.CANCEL;
		}
		
		return alertResult;
	}
}
