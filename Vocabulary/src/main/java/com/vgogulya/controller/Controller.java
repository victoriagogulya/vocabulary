package com.vgogulya.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

import java.util.List;

import com.vgogulya.model.Constants;
import com.vgogulya.model.Database;
import com.vgogulya.model.Word;
import com.vgogulya.model.dbTable.eDBTableType;
import com.vgogulya.view.Utility;
import com.vgogulya.view.eAlertResult;

public class Controller {
	
	private Database database = new Database();
	
	@FXML
	private Button buttonTranslate;
	
	@FXML
	private Button buttonAdd;
	
	@FXML
	private Button buttonRemove;
	
	@FXML
	private Button buttonClean;
	
	@FXML
	private TextField textFieldInput;
	
	@FXML
	private TextArea textFieldOutput;
	
	@FXML
	private void handlerButtonTranslateClicked(ActionEvent event){
		
		String word = textFieldInput.getText();
		if(Word.isCorrect(word)){
			word = Word.toLowerCase(word);
			setDBTable(word);
			List<String> translatedWords = null;
			if(database.connect()){
				translatedWords = database.select(word);
				database.closeConnection();
			}
			else{
				Utility.showAlertInfo("ERROR. No connection to DB");
			}
			fillTextFieldOutput(translatedWords);
			textFieldInput.setText(word);
		}
		else{
			Utility.showAlertInfo("ERROR. Wrong word for translation");
		}
	}
	
	@FXML
	private void handlerButtonAddClicked(ActionEvent event){
		String wordIn = textFieldInput.getText();
		String wordOut = textFieldOutput.getText();
		if(Word.isCorrect(wordIn)){
			wordIn = Word.toLowerCase(wordIn);
			
			List<String> listOfWords = Word.splitToWords(wordOut, Constants.SPLITTER_N);
			if(Word.areCorrectAll(listOfWords)){
				listOfWords = Word.toLowerCaseAll(listOfWords);
				setDBTable(wordIn);
				if(database.connect()){
					int insertedRows = database.insert(wordIn, listOfWords);
					Utility.showAlertInfo("Inserted " + insertedRows + " records");
					database.closeConnection();
				}
				else{
					Utility.showAlertInfo("ERROR. No connection to DB");
				}
			}
		}
		else{
			Utility.showAlertInfo("ERROR. Wrong word for translation");
		}
	}
	
	@FXML
	private void handlerButtonRemoveClicked(ActionEvent event){
		String wordIn = textFieldInput.getText();
		String wordOut = textFieldOutput.getText();
		int deletedRows = 0;
		
		if(Word.isCorrect(wordIn)){
			wordIn = Word.toLowerCase(wordIn);
			
			eAlertResult result = Utility.showAlertConfirmation("REMOVE records for " + wordIn + "in DB ?");
			if(eAlertResult.OK == result){
				if(database.connect()){
					setDBTable(wordIn);
					if(wordOut.isEmpty()){
						deletedRows = database.deleteAll(wordIn);
					}
					else{
						List<String> listOfWords = Word.splitToWords(wordOut, Constants.SPLITTER_N);
						if(Word.areCorrectAll(listOfWords)){
							listOfWords = Word.toLowerCaseAll(listOfWords);
							
							for(String str : listOfWords){
								deletedRows += database.delete(wordIn, str);
							}
						}
					}
					database.closeConnection();
					Utility.showAlertInfo("Removed " + deletedRows + " records ");
				}
				else{
					Utility.showAlertInfo("ERROR. No connection to DB");
				}
			}
		}
		else{
			Utility.showAlertInfo("ERROR. Wrong word for translation");
		}
	}
	
	@FXML
	private void handlerButtonCleanClicked(ActionEvent event){
		textFieldInput.clear();
		textFieldOutput.clear();
	}
	
	private void fillTextFieldOutput(List<String> translatedWords){
		if(null != translatedWords && !translatedWords.isEmpty()){
			textFieldOutput.clear();
			for(String str : translatedWords){
				textFieldOutput.appendText(str);
				textFieldOutput.appendText(Constants.SPLITTER_N);
			}
		}
		else{
			Utility.showAlertInfo("No translation. Add new word to DB");
		}
	}
	
	private void setDBTable(String word){
		if(Word.isEnglish(word)){
			database.setCurrentDBTable(eDBTableType.ENG_RUS);
		}
		else{
			database.setCurrentDBTable(eDBTableType.RUS_ENG);
		}
	}
}
